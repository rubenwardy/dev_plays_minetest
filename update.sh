#!/bin/bash

for i in *; do
	if [ -f $i/.git ]; then
		echo Updating $i
		pushd $i
		git pull origin master
		popd
	fi
done
